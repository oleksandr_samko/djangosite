from django.contrib import admin
from .models import *

class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 0

# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Product._meta.fields]
    list_filter = ('name','is_active','created','updated')
    search_fields = ['name']
    inlines = [ProductImageInline]

    class Meta:
        model = Product

admin.site.register(Product, ProductAdmin)

class ProductImageAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ProductImage._meta.fields]
    list_filter = ('product','is_active','created','updated')
    search_fields = ['product','image']

    class Meta:
        model = ProductImage

admin.site.register(ProductImage, ProductImageAdmin)

