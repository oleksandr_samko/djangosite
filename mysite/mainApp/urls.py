from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('landing/', views.landing, name='landing'),
    path('debug/', views.DebugTest, name='DebugTest')
]
