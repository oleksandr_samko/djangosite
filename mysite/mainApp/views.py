from django.shortcuts import render
from .forms import SubscriberForm
from products.models import Product

# Create your views here.
def home(request):
    products = Product.objects.filter(is_active=True)
    return render(request, 'mainApp/home.html', locals())


def landing(request):
    print(request.method)
    if request.method == 'POST':
        form = SubscriberForm(request.POST)         
        if(form.is_valid()):   
            form = form.save()
            #refresh
    else:
        form = SubscriberForm(None)    
    return render(request, 'mainApp/landing.html', locals())

def DebugTest(request):
    if request.method == 'POST':
        form = SubscriberForm(request.POST)
        if form.is_valid():
            pass  # does nothing, just trigger the validation
    else:
        form = SubscriberForm()
    return render(request, 'mainApp/debug.html', {'form': form})